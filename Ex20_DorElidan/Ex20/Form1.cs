﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace Ex20
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string card_current;
        bool card_flipped = false;
        NetworkStream clientStream;      

        public void reset()
        {
            foreach (Control x in this.Controls)
            {
                if (x is PictureBox)
                {
                    ((PictureBox)x).Image = Image.FromFile("PNG-cards/card back red.png");
                }
            }
            pictureBox11.Image = Image.FromFile("PNG-cards/card back blue.png");
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TcpClient tcpClientC = new TcpClient();

            Int32 port = 8820;
            IPAddress ip = IPAddress.Parse("127.0.0.1");

            tcpClientC.Connect(ip, port);

            clientStream = tcpClientC.GetStream();

            Thread t = new Thread(() => listen(clientStream));
            t.Start();

            reset();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Change(false, true);
            string[] dir = Directory.GetFiles("PNG-cards/Cards");
            int randomNumber = new Random().Next(0,52);
            (sender as PictureBox).Image = Image.FromFile(dir[randomNumber]);
            string parsedCardName = parseCard(dir[randomNumber]);

            card_current = parsedCardName;
            card_flipped = true;
            
            Send("1"+parsedCardName);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            
        }

        public void Send(string message)
        {
            byte[] buffer = new ASCIIEncoding().GetBytes(message);
            clientStream.Write(buffer, 0, 4);
            clientStream.Flush();
        }

        static string Recieve(NetworkStream clientStream)
        {
            byte[] bufferIn = new byte[4];
            int bytesRead = clientStream.Read(bufferIn, 0, 4);
            return new ASCIIEncoding().GetString(bufferIn);
        }
        public void listen(NetworkStream clientStream)
        {
            Change(false, false);
            string message;

            while (true)
            {
                string cardImage = "PNG-cards", opCurrentCard;
                message = Recieve(clientStream);
                if (message[0] == '0') //game starts
                {
                    Change(true, false);
                }
                else if (message[0] == '1')
                {
                    opCurrentCard = message.Substring(1, 3);

                    foreach (string path in Directory.GetFiles("PNG-cards/Cards"))
                    {
                        if (parseCard(path) == opCurrentCard)
                        {
                            cardImage = path;
                        }
                    }

                    while (true) //waiting for player to select his card
                    {
                        if (card_flipped)
                        {
                            pictureBox11.Image = Image.FromFile(cardImage);
                            int myCard = Int32.Parse(card_current.Substring(0, 2)), opCard = Int32.Parse(opCurrentCard.Substring(0, 2));
                            
                            if (myCard > opCard)
                            {
                                MyScoreCount.Invoke(new Action(() => MyScoreCount.Text = (Int32.Parse(MyScoreCount.Text)+1).ToString()));
                            }
                            else if (myCard < opCard)
                            {
                                OpScoreCount.Invoke(new Action(() => OpScoreCount.Text = (Int32.Parse(OpScoreCount.Text) + 1).ToString()));
                            }

                            Thread.Sleep(3000);
                            reset();
                            Change(true, false);
                            card_flipped = false;
                            break;
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Bye Bye =(");
                    this.Close();
                }
            }
        }

        public string parseCard(string cardName)
        {
            string returnVal = "000";
            string[] cardTypes = {"ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "jack", "queen", "king"};
            string[] cardTypesParsed = { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13"};

            int i = 0;
            foreach(string a in cardTypes)
            {
                if (cardName.IndexOf(a) >= 0) 
                {
                    returnVal = cardTypesParsed[i];
                }
                i++;
            }

            if (cardName.IndexOf("clubs") >= 0)
            {
                returnVal += "C";
            }
            else if (cardName.IndexOf("diamonds") >= 0)
            {
                returnVal += "D";
            }
            else if (cardName.IndexOf("hearts") >= 0)
            {
                returnVal += "H";
            }
            else
            {
                returnVal += "S";
            }

            return returnVal;
        }

        void Change(bool enabled, bool lockQuitButton)
        {
            foreach (Control c in this.Controls)
            {
                if ((!lockQuitButton || !(c is Button)) && !(c is Label))
                    c.Invoke(new Action (() => c.Enabled = enabled));
            }
        } 

        private void QuitButton_Click(object sender, EventArgs e)
        {
            Send("2000");
            MessageBox.Show("Bye Bye =(");
            this.Close();
        }
    }
}
